package ru.ahmetahunov.tm.exception;

public final class InterruptOperationException extends Exception {

    public InterruptOperationException(String s) {
        super(s);
    }

}

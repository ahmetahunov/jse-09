package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.entity.IItem;
import ru.ahmetahunov.tm.entity.User;

public final class InfoUtil {

    @NotNull
    public static String getItemInfo(@NotNull final IItem item) {
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Name: ");
        sb.append(item.getName());
        sb.append("\nDescription: ");
        sb.append(item.getDescription());
        sb.append("\nStart date: ");
        @NotNull String date = DateUtil.formatDate(item.getStartDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        sb.append("\nFinish date: ");
        date = DateUtil.formatDate(item.getFinishDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        sb.append("\nStatus: ");
        sb.append(item.getStatus().getDisplayName());
        sb.append("\nID: ");
        sb.append(item.getId());
        return sb.toString();
    }

    @NotNull
    public static String getUserInfo(@NotNull final User user) {
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Login: ");
        sb.append(user.getLogin());
        sb.append("\nUser status: ");
        sb.append(user.getRole().getDisplayName());
        sb.append("\nUser id: ");
        sb.append(user.getId());
        return sb.toString();
    }

}

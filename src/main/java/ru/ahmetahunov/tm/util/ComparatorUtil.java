package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.entity.IItem;
import java.util.Comparator;

public final class ComparatorUtil {

    @NotNull
    public static <T extends IItem> Comparator<T> getComparator(@NotNull final String comparatorName) {
        if ("startDate".equalsIgnoreCase(comparatorName)) return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        };
        if ("finishDate".equalsIgnoreCase(comparatorName)) return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getFinishDate().compareTo(o2.getFinishDate());
            }
        };
        if ("status".equalsIgnoreCase(comparatorName)) return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getStatus().ordinal() - o2.getStatus().ordinal();
            }
        };
        if ("creationDate".equalsIgnoreCase(comparatorName)) return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getCreationDate().compareTo(o2.getCreationDate());
            }
        };
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
    }

}

package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.entity.IItem;
import ru.ahmetahunov.tm.enumerated.Status;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity implements IItem {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = new Date(0);

    @NotNull
    private Date finishDate = new Date(0);

    @NotNull
    private Date creationDate = new Date();

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private String userId = "";

}

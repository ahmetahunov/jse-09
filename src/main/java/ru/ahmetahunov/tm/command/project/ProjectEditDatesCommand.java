package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectEditDatesCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-edit-dates";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit start and finish dates in selected project.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DATES]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectId, user.getId());
        if (project == null) throw new InterruptOperationException("Selected project does not exist.");
        @NotNull final String startDate = terminalService.getAnswer("Please enter new start date: ");
        @NotNull final String finishDate = terminalService.getAnswer("Please enter new finish date: ");
        project.setStartDate(DateUtil.parseDate(startDate));
        project.setFinishDate(DateUtil.parseDate(finishDate));
        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}

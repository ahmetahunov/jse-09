package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "project-description";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show selected project information.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[PROJECT-DESCRIPTION]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectId, user.getId());
        if (project == null) throw new InterruptOperationException("Selected project does not exist.");
        terminalService.writeMessage(InfoUtil.getItemInfo(project));
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}

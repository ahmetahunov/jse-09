package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

@NoArgsConstructor
public final class TaskDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "task-description";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show selected task description.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        terminalService.writeMessage("[TASK-DESCRIPTION]");
        @NotNull final String taskId = terminalService.getAnswer("Please enter task id: ");
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskId, user.getId());
        if (task == null) throw new InterruptOperationException("Selected task does not exist.");
        terminalService.writeMessage(InfoUtil.getItemInfo(task));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}

package ru.ahmetahunov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

import java.util.List;

public class TaskFindCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-find";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search task by part of name or description.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[FIND TASK]");
        @NotNull final String searchPhrase = terminalService.getAnswer("Please enter task name or description: ");
        @NotNull final List<Task> tasks = taskService.findByNameOrDesc(searchPhrase, user.getId());
        int i = 1;
        for (@NotNull Task task : tasks) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            task.getName(),
                            task.getId(),
                            task.getDescription())
            );
        }
        if (tasks.isEmpty()) terminalService.writeMessage("Not found.");
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}

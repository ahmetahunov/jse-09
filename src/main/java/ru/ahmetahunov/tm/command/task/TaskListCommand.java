package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import java.io.IOException;
import java.util.Comparator;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available tasks.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASK LIST]");
        @NotNull final String comparatorName =
                terminalService.getAnswer("Enter sort type<creationDate|startDate|finishDate|status>: ");
        @NotNull final Comparator<Task> comparator = ComparatorUtil.getComparator(comparatorName);
        int i = 1;
        for (@NotNull Task task : taskService.findAll(user.getId(), comparator)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, task.getName(), task.getId()));
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
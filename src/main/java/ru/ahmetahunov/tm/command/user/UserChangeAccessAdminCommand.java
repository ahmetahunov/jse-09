package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.RoleUtil;
import java.io.IOException;

@NoArgsConstructor
public final class UserChangeAccessAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "change-user-access";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change selected user's access.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[CHANGE USER'S ACCESS RIGHTS]");
        @NotNull final String login = terminalService.getAnswer("Please enter user's login: ");
        @Nullable final User user = userService.findUser(login);
        if (user == null) throw new InterruptOperationException("Selected user does not exist.");
        @NotNull final String answer = terminalService.getAnswer("Please enter role<User/Administrator>: ");
        @Nullable final Role role = RoleUtil.getRole(answer);
        if (role == null) throw new InterruptOperationException("Unknown role.");
        user.setRole(role);
        serviceLocator.getUserService().merge(user);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}

package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.PassUtil;
import ru.ahmetahunov.tm.util.RoleUtil;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserRegistrationAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-register-admin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "New user registration with role setting.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException, NoSuchAlgorithmException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminalService.writeMessage("[REGISTRATION ADMIN]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        if (userService.contains(login))
            throw new InterruptOperationException("This login already exists. Try one more time!");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new InterruptOperationException("Passwords do not match!");
        password = PassUtil.getHash(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        @NotNull String answer = terminalService.getAnswer("Please enter role<User/Administrator>: ");
        @Nullable final Role role = RoleUtil.getRole(answer);
        if (role == null) terminalService.writeMessage("Unknown role. Default role is set.");
        else user.setRole(role);
        userService.persist(user);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}

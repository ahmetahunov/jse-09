package ru.ahmetahunov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IRepository;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> collection = new HashMap<>();

    @NotNull
    @Override
    public T persist(@NotNull final T item) throws IdCollisionException {
        if (collection.containsKey(item.getId())) throw new IdCollisionException();
        collection.put(item.getId(), item);
        return item;
    }

    @NotNull
    @Override
    public T merge(@NotNull final T item) {
        collection.put(item.getId(), item);
        return item;
    }

    @NotNull
    @Override
    public T remove(@NotNull final String id) {
        return collection.remove(id);
    }

    @NotNull
    @Override
    public Collection<T> findAll() {
        return collection.values();
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {
        return collection.get(id);
    }

}

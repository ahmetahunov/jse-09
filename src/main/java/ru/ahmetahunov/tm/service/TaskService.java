package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.entity.Task;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository repository;

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @NotNull final Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAll(projectId, userId);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String taskId, @Nullable final String userId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(taskId, userId);
    }

    @NotNull
    @Override
    public List<Task> findByName(@Nullable final String taskName, @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findByName(taskName, userId);
    }

    @NotNull
    @Override
    public List<Task> findByDescription(@Nullable final String description, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null) return Collections.emptyList();
        return repository.findByDescription(description, userId);
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDesc(@Nullable final String searchPhrase, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        return repository.findByNameOrDesc(searchPhrase, userId);
    }

    @Override
    public void removeAllProjectTasks(@Nullable final String projectId, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        for (@NotNull Task task : repository.findAll(projectId, userId)) {
            repository.remove(task.getId());
        }
    }

    @Nullable
    @Override
    public Task remove(@Nullable final String taskId, @Nullable final String userId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (repository.contains(taskId, userId)) return repository.remove(taskId);
        return null;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

}

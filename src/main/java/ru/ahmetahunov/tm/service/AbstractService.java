package ru.ahmetahunov.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IAbstractService;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.api.repository.IRepository;
import java.util.UUID;

@RequiredArgsConstructor
public class AbstractService<T extends AbstractEntity> implements IAbstractService<T> {

    @NotNull
    private final IRepository<T> repository;

    @Nullable
    @Override
    public T persist(@Nullable final T item) {
        if (item == null) return null;
        if (item.getId().isEmpty()) return null;
        try {
            repository.persist(item);
            return item;
        } catch (IdCollisionException e) {
            item.setId(UUID.randomUUID().toString());
            return persist(item);
        }
    }

    @Nullable
    @Override
    public T merge(@Nullable final T item) {
        if (item == null) return null;
        if (item.getId().isEmpty()) return null;
        return repository.merge(item);
    }

    @Nullable
    @Override
    public T remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.remove(id);
    }

    @Nullable
    @Override
    public T remove(@Nullable final T item) {
        if (item == null) return null;
        if (item.getId().isEmpty()) return null;
        return repository.remove(item.getId());
    }

    @Nullable
    @Override
    public T findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOne(id);
    }

}

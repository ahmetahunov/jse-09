package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository repository;

    public UserService(@NotNull final IUserRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Nullable
    @Override
    public User findUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return repository.findUser(login);
    }

    @NotNull
    @Override
    public Collection<User> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean contains(@Nullable final String login) {
        if (login == null || login.isEmpty()) return true;
        return repository.contains(login);
    }

}

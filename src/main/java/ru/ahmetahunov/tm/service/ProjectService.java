package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.entity.Project;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository repository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.repository = projectRepository;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(id, userId);
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findByName(projectName, userId);
    }

    @NotNull
    @Override
    public List<Project> findByDescription(@Nullable final String description, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null) return Collections.emptyList();
        return repository.findByDescription(description, userId);
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(@Nullable final String searchPhrase, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        return repository.findByNameOrDesc(searchPhrase, userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @NotNull Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

    @Nullable
    @Override
    public Project remove(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (repository.contains(projectId, userId)) return repository.remove(projectId);
        return null;
    }

}

package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import java.util.Collection;

public interface IUserService extends IAbstractService<User> {

    @Nullable
    public User findUser(String login);

    @NotNull
    public Collection<User> findAll();

    public boolean contains(String login);

}
